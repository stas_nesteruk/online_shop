import express from 'express';
const router = express.Router();
import {
    getProducersTreatment,
    createProducerTreatment,
    deleteProducerTreatment,
    updateProducerTreatment
} from './producer.controller';

import {
    auth,
    checkPermission
} from '../../middleware';

router.get('/producers', getProducersTreatment);
router.post('/producers', auth, checkPermission, createProducerTreatment);

router.delete('/producers/:id', auth, checkPermission, deleteProducerTreatment);
router.patch('/producers/:id', auth, checkPermission, updateProducerTreatment);

export { router as producerRouter };
