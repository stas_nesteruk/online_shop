import HTTP_STATUS from 'http-status';
import { getProduct } from '../product/product.service';
import { getCategories } from '../category';
import { sendJsonData, sendJsonError } from '../../utils/response';

export async function getCartTreatment(req, res) {
    try {
        const cart = {};
        cart.products = req.session.cart.products;
        cart.totalCost = req.session.cart.totalCost;
        cart.totalCount = req.session.cart.totalCount;
        sendJsonData(res, { cart }, HTTP_STATUS.OK);
    } catch (error) {
        sendJsonError(res, error, HTTP_STATUS.INTERNAL_SERVER_ERROR);
    }
}

export async function addProductToCartTreatment(req, res) {
    if (!req.body.id || !req.body.count) {
        return sendJsonError(res, { message: 'Wrong required field' }, HTTP_STATUS.BAD_REQUEST);
    }
    try {
        const product = await getProduct(req.body.id);
        if (!product) {
            return sendJsonError(res, { message: 'Product didn\'t find' }, HTTP_STATUS.NOT_FOUND);
        }
        const index = req.session.cart.products.findIndex(
            product => product.product._id === req.body.id
        );
        let totalCost;
        let totalCount;
        if (index < 0) {
            req.session.cart.products.push({
                product,
                count: req.body.count
            });
        } else {
            req.session.cart.products[index].count += req.body.count;
        }
        totalCost = req.session.cart.totalCost;
        totalCount = req.session.cart.totalCount;
        totalCost += product.price * req.body.count;
        totalCount += req.body.count;
        req.session.cart.totalCost = totalCost;
        req.session.cart.totalCount = totalCount;
        sendJsonData(res, { totalCost, totalCount }, HTTP_STATUS.OK);
    } catch (error) {
        if(error.name === 'CastError'){
            return sendJsonError(res, {message: 'Wrong id number. Id must be a single String of 12 bytes or a string of 24 hex characters.'}, HTTP_STATUS.BAD_REQUEST);
        }
        sendJsonError(res, error, HTTP_STATUS.INTERNAL_SERVER_ERROR);
    }
}

export async function deleteProductFromCartTreatment(req, res){
    if(!req.body.id || !req.body.count){
        return sendJsonError(res, { message: 'Wrong required field' }, HTTP_STATUS.BAD_REQUEST);
    }
    try{
        const product = await getProduct(req.body.id);
        if(!product){
            return sendJsonError(res, { message: 'Product didn\'t find' }, HTTP_STATUS.NOT_FOUND);
        }
        const index = req.session.cart.products.findIndex(
            product => product.product._id === req.body.id
        )
        if(index < 0){
            return sendJsonError(res, { message: 'Wrong product id' }, HTTP_STATUS.BAD_REQUEST);
        }
        let count = req.session.cart.products[index].count;
        let totalCount = req.session.cart.totalCount;
        let totalCost = req.session.cart.totalCost;
        count -= req.body.count;
        if(count < 0){
            return sendJsonError(res, { message: 'Wrong count number' }, HTTP_STATUS.BAD_REQUEST);
        }
        if(count === 0){
            req.session.cart.products.splice(index, 1); 
        }else{
            req.session.cart.products[index].count -= req.body.count;
        }
        totalCount -= req.body.count;
        totalCost -= product.price * req.body.count;
        req.session.cart.totalCost = totalCost;
        req.session.cart.totalCount = totalCount;
        sendJsonData(res, { totalCost, totalCount }, HTTP_STATUS.OK);
    }catch(error){
        if(error.name === 'CastError'){
            return sendJsonError(res, {message: 'Wrong id number. Id must be a single String of 12 bytes or a string of 24 hex characters.'}, HTTP_STATUS.BAD_REQUEST);
        }
        sendJsonError(res, error, HTTP_STATUS.INTERNAL_SERVER_ERROR);
    }
}