import HTTP_STATUS from 'http-status';

export function sendJsonError(res, error, status = HTTP_STATUS.BAD_REQUEST) {
    res.status(status).json({
        success: false,
        error
    });
}

export function sendValidationError(res, validation) {
    res.status(HTTP_STATUS.BAD_REQUEST).json({
        success: false,
        validation
    });
}

export function sendJsonData(res, data = {}, status = HTTP_STATUS.OK) {
    res.status(status).json(
        Object.assign(
            {
                success: true
            },
            data
        )
    );
}

export function sendUnauthorizedError(res) {
    const error = {};
    error.message = `Unauthorized access at path ${res.req.url}`;
    sendJsonError(res, error, HTTP_STATUS.UNAUTHORIZED);
}
