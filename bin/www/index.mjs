import express from 'express';
import session from 'express-session';
import connectMongo from 'connect-mongo';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import mongoose from '../../src/db/mongoose';
import path from 'path';
import { apiRouter } from '../../src/router';
import { sendJsonError, sendJsonData } from '../../src/utils/response';

const MongoStore = connectMongo(session);

const app = express();
const router = express.Router();

const publicDirectoryPath = path.join(process.cwd(), '/public');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(function(req, res, next) {
    const allowedOrigins = ['http://localhost:3000', 'https://nesteruk-shop-ui.herokuapp.com'];
    const origin = req.headers.origin;
    if(allowedOrigins.indexOf(origin) > -1){
        res.setHeader('Access-Control-Allow-Origin', origin);
    }   
    res.header('Access-Control-Allow-Methods', 'GET,PATCH,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Cache', 'no-cache');
    return next();
  });


app.use(
    session({
        name: 'sid',
        secret: 'keyboard cat',
        saveUninitialized: false,
        resave: false,
        store: new MongoStore({
            mongooseConnection: mongoose.connection,
            autoRemove: 'interval',
            autoRemoveInterval: 10
        }),
        cookie: {
            sameSite: 'none',
            maxAge: 1000 * 60 * 60 * 24,
        },
    })
);

app.use(cookieParser('api secret key'));
app.use((req, res, next) => {
    if (!req.session.cart) {
        req.session.cart = {
            totalCost: 0,
            totalCount: 0,
            products: []
        };
    }
    res.locals.session = req.session;
    console.log('IT\'S SERVER SIDE');
    next();
});

app.use(express.static(publicDirectoryPath));

router.get('/', (req, res) => {
    sendJsonData(res, { message: 'Greetings to you, mortal!'}, HTTP_STATUS.OK);
});

router.get('*', (req, res) => {
    sendJsonError(res, { message: 'Ups, something went wrong..' });    
});

app.use(apiRouter);
app.use(router);

export default app;
