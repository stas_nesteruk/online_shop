import HTTP_STATUS from 'http-status';
import sharp from 'sharp';
import path from 'path';
import fs from 'fs';
import {
    getProducts,
    getProductsByCategory,
    getProduct,
    searchProducts,
    saveProduct,
    deleteProductById,
    updateProduct
} from './product.service';
import { getCategories, getCategoryByName } from './../category';
import { getProducers, getProducerByName } from './../producer';
import { sendJsonError, sendJsonData } from '../../utils/response/';

export async function getProductsTreatment(req, res) {
    const sort = {};
    try {
        if(req.query.sortBy){
            const parts = req.query.sortBy.split(':');
            sort[parts[0]] = parts[1] === 'desc' ? -1 : 1;    
        }
        const products = await getProducts(parseInt(req.query.limit), parseInt(req.query.skip), sort);
        sendJsonData(res, { products }, HTTP_STATUS.OK);
    } catch (error) {
        sendJsonError(res, error, HTTP_STATUS.INTERNAL_SERVER_ERROR);
    }
}

export async function getProductsByCategoryTreatment(req, res) {
    const category = req.params.category;
    try {
        const products = await getProductsByCategory(category);
        if(typeof products === 'undefined'){
            return sendJsonError(res, { message: 'Wrong category name' }, HTTP_STATUS.BAD_REQUEST);
        }
        sendJsonData(res, { products }, HTTP_STATUS.OK);
    } catch (error) {
        sendJsonError(res, error, HTTP_STATUS.INTERNAL_SERVER_ERROR);
    }
}

export async function getProductTreatment(req, res) {
    const id = req.params.id;
    try {
        const product = await getProduct(id);
        if (!product) {
            return sendJsonError(res, { message: `Product didn\'t find by ID: ${{id}}` } , HTTP_STATUS.NOT_FOUND);
        }
        sendJsonData(res, { product }, HTTP_STATUS.OK);
    } catch (error) {
        if(error.name === 'CastError'){
            return sendJsonError(res, {message: 'Wrong id number. Id must be a single String of 12 bytes or a string of 24 hex characters.'}, HTTP_STATUS.BAD_REQUEST);
        }
        sendJsonError(res, error, HTTP_STATUS.INTERNAL_SERVER_ERROR);
    }
}

export async function searchProductsTreatment(req, res) {
    let query = {};
    let products = [];
    if (!req.query.value) {
        return sendJsonError(res, { message: 'Wrong query string' }, HTTP_STATUS.BAD_REQUEST);
    }
    try {
        query = req.query.value;
        products = await searchProducts(query);
        sendJsonData(res, { products }, HTTP_STATUS.OK);
    } catch (error) {
        sendJsonError(res, error, HTTP_STATUS.INTERNAL_SERVER_ERROR);
    }
}

export async function deleteProductByIdTreatment(req, res) {
    const id = req.params.id;
    try {
        const product = deleteProductById(id);
        if(!product){
            return sendJsonError(res, { message: 'Product doesn\'t exist' }, HTTP_STATUS.NOT_FOUND);
        }
        sendJsonData(res, { product }, HTTP_STATUS.OK);
    } catch (error) {
        if(error.name === 'CastError'){
            return sendJsonError(res, {message: 'Wrong id number. Id must be a single String of 12 bytes or a string of 24 hex characters.'}, HTTP_STATUS.BAD_REQUEST);
        }
        sendJsonError(res, error, HTTP_STATUS.INTERNAL_SERVER_ERROR);
    }
}

async function saveImage(file, category) {
    const categoryPath = category.name.replace(' ', '-').toLowerCase();
    const publicFolderName = '/public';
    const dest = '/media/' + categoryPath + '/';
    const nm = file.originalname.match(/(.+?)(\.[^.]*$|$)/);
    const imageName = nm[1] + Date.now() + '.png';
    if (!fs.existsSync(path.join(process.cwd(), publicFolderName + dest))) {
        fs.mkdirSync(path.join(process.cwd(), publicFolderName + dest));
    }
    const imagePath = path.join(process.cwd(), publicFolderName + dest + imageName);
    await sharp(file.buffer)
        .resize({ width: 400, height: 400 })
        .png()
        .toFile(imagePath);
    return dest + imageName;
}

export async function createProductTreatment(req, res) {
    const requiredFields = [
        'name',
        'description',
        'price',
        'producer',
        'category'
    ];
    const createKeys = Object.keys(req.body);
    const isValidOperation = requiredFields.every(field =>
        createKeys.includes(field)
    );
    if (!isValidOperation) {
        return sendJsonError(res, {message: 'Wrong required fields.'});
    }
    try {
        const { producer, category } = req.body;
        const findedProducer = await getProducerByName(producer);
        if (!findedProducer) {
            return sendJsonError(res, {message: 'Wrong producer.'});
        }
        const findedCategory = await getCategoryByName(category);
        if (!findedCategory) {
            return sendJsonError(res, {message: 'Wrong category.'});
        }
        const image = await saveImage(req.file, findedCategory);
        const data = req.body;
        data.producer = findedProducer;
        data.category = findedCategory;
        data.image = image;
        let count;
        if (!req.body.count) {
            count = 0;
        } else {
            count = req.body.count;
        }
        data.count = count;
        const product = await saveProduct(data);
        console.log(product);
        sendJsonData(res, {product}, HTTP_STATUS.CREATED)
    } catch (error) {
        res.status(HTTP_STATUS.INTERNAL_SERVER_ERROR).send(error);
    }
}

export async function updateProductTreatment(req, res) {
    const id = req.params.id;
    const updatesKeys = Object.keys(req.body);
    const allowedUpdates = [
        'name',
        'description',
        'price',
        'producer',
        'category',
        'count'
    ];
    const isValidOperation = updatesKeys.every(key =>
        allowedUpdates.includes(key)
    );
    if (!isValidOperation && !req.file) {
        return sendJsonError(res, { message: 'Data consist doesn\'t allow fields'}, HTTP_STATUS.BAD_REQUEST)
    }
    try {
        const data = req.body;
        const product = await getProduct(id);
        if(!product){
            return sendJsonError(res, {message: 'Product doesn\'t exist'}, HTTP_STATUS.NOT_FOUND);
        }
        if(req.body.producer){
            const producer = await getProducerByName(req.body.producer);
            if(!producer){
                return sendJsonError(res, {message: 'Wrong producer'}, HTTP_STATUS.BAD_REQUEST);
            }
            data.producer = producer;
        }
        let category;
        if(req.body.category){
            category = await getCategoryByName(req.body.category);
            if(!category){
                return sendJsonError(res, {message: 'Wrong category'}, HTTP_STATUS.BAD_REQUEST);
            }
            data.category = category;
        }else{
            category = product.category;
        } 
        let image;
        if(req.file){
            image = await saveImage(req.file, category);
            updatesKeys.push('image');
            data.image = image;
        }
        const updatedProduct = await updateProduct(id, updatesKeys, data);
        sendJsonData(res, {updatedProduct}, HTTP_STATUS.OK);
    } catch (error) {
        if(error.name === 'CastError'){
            return sendJsonError(res, {message: 'Wrong id number. Id must be a single String of 12 bytes or a string of 24 hex characters.'}, HTTP_STATUS.BAD_REQUEST);
        }
        sendJsonError(res, error, HTTP_STATUS.INTERNAL_SERVER_ERROR);
    }
}
