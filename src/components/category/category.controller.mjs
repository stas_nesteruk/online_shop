import HTTP_STATUS from 'http-status';
import { 
    getCategories,
    saveCategory,
    deleteCategory,
    updateCategoryById
} from './category.service';
import { sendJsonData, sendJsonError } from '../../utils/response';

export async function getCategoriesTreatment(req, res){
    try{
        const categories = await getCategories();
        sendJsonData(res, { categories }, HTTP_STATUS.OK);
    }catch(error){
        sendJsonError(res, error, HTTP_STATUS.INTERNAL_SERVER_ERROR);
    }
}

export async function deleteCategoryTreatment(req, res){
    const id = req.params.id;
    try{
        const category = deleteCategory(id);
        if(typeof category === 'undefined'){
            sendJsonError(res, { message: 'Category didn\'t find'}, HTTP_STATUS.NOT_FOUND);
        }
        sendJsonData(res, { category }, HTTP_STATUS.OK);
    }catch(error){
        if(error.name === 'CastError'){
            return sendJsonError(res, {message: 'Wrong id number. Id must be a single String of 12 bytes or a string of 24 hex characters.'}, HTTP_STATUS.BAD_REQUEST);
        }
        sendJsonError(res, error, HTTP_STATUS.INTERNAL_SERVER_ERROR);
    }
}

export async function updateCategoryTreatment(req, res){
    const id = req.params.id;
    if (!req.body.name) {
        return sendJsonError(res, { message: 'Wrong required field' }, HTTP_STATUS.BAD_REQUEST);
    }
    try{
        const category = await updateCategoryById(id, req.body.name);
        sendJsonData(res, { category }, HTTP_STATUS.OK);
    }catch(error){
        if(error.name === 'CastError'){
            return sendJsonError(res, {message: 'Wrong id number. Id must be a single String of 12 bytes or a string of 24 hex characters.'}, HTTP_STATUS.BAD_REQUEST);
        }
        sendJsonError(res, error, HTTP_STATUS.INTERNAL_SERVER_ERROR);
    }
}

export async function createCategoryTreatment(req, res){
    try{
        if(!req.body.name){
            return sendJsonError(res, { message: 'Wrong required field' }, HTTP_STATUS.BAD_REQUEST);
        }
        const category = await saveCategory(req.body.name);
        sendJsonData(res, { category }, HTTP_STATUS.CREATED);
    }catch(error){
        sendJsonError(res, error, HTTP_STATUS.INTERNAL_SERVER_ERROR);
    }
}