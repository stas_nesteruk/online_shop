import HTTP_STATUS from 'http-status';
import jwt from 'jsonwebtoken';
import sharp from 'sharp';
import path from 'path';
import fs from 'fs';
import {
    findUserByCredential,
    saveUser,
    deleteUser,
    updateUser
} from './user.service';
import { sendJsonData, sendJsonError } from '../../utils/response';

export async function loginUserTreatment(req, res) {
    try {
        const { email, password } = req.body;
        const user = await findUserByCredential(email, password);
        if(!user){
            return sendJsonError(res, {message: 'Unable to login'}, HTTP_STATUS.BAD_REQUEST);
        }
        const token = await generateToken(user);
        sendJsonData(res, { user, token }, HTTP_STATUS.OK);
    } catch (error) {
        sendJsonError(res, error, HTTP_STATUS.INTERNAL_SERVER_ERROR);
    }
}

async function saveImage(file, email) {
    const publicFolderName = '/public';
    const dest = '/media/profileImages/' + email + '/';
    const nm = file.originalname.match(/(.+?)(\.[^.]*$|$)/);
    const imageName = nm[1] + Date.now() + '.png';
    if (!fs.existsSync(path.join(process.cwd(), publicFolderName + dest))) {
        fs.mkdirSync(path.join(process.cwd(), publicFolderName + dest));
    }
    const imagePath = path.join(
        process.cwd(),
        publicFolderName + dest + imageName.replace(' ', '_')
    );
    await sharp(file.buffer)
        .resize({ width: 400, height: 400 })
        .png()
        .toFile(imagePath);
    return dest + imageName;
}

const DEFAULT_FEMALE_IMAGE = '/media/profileImages/default/female.jpg';
const DEFAULT_MALE_IMAGE = '/media/profileImages/default/male.jpg';

export async function signupUserTreatment(req, res) {
    const requiredFields = ['name', 'gender', 'email', 'password'];
    const keys = Object.keys(req.body);
    const isValidOperation = requiredFields.every(field =>
        keys.includes(field)
    );
    if (!isValidOperation) {
        return sendJsonError(res, { message: 'Wrong required fields.'}, HTTP_STATUS.BAD_REQUEST);
    }
    try {
        const data = req.body;
        let image;
        if (req.file) {
            image = await saveImage(req.file, data.email);
        } else {
            image =
                data.gender === 'Male'
                    ? DEFAULT_MALE_IMAGE
                    : DEFAULT_FEMALE_IMAGE;
        }
        data.image = image;
        const user = await saveUser(data);
        const token = await generateToken(user);
        sendJsonData(res, { user, token }, HTTP_STATUS.CREATED);
    } catch (error) {
        sendJsonError(res, error, HTTP_STATUS.INTERNAL_SERVER_ERROR);
    }
}

export async function logoutUserTreatment(req, res) {
    try {
        req.session.user.tokens = req.session.user.tokens.filter(
            token => token.token != req.session.token
        );
        await req.session.user.save();
        delete req.session.user;
        sendJsonData(res, {}, HTTP_STATUS.OK);
    } catch (error) {
        sendJsonError(res, error, HTTP_STATUS.INTERNAL_SERVER_ERROR);
    }
}

export async function logoutAllUserTreatment(req, res) {
    try {
        req.session.user.tokens = [];
        await req.session.user.save();
        delete req.session.user;
        sendJsonData(res, {}, HTTP_STATUS.OK);
    } catch (error) {
        sendJsonError(res, error, HTTP_STATUS.INTERNAL_SERVER_ERROR);
    }
}

function generateToken(user) {
    const token = jwt.sign(
        { _id: user._id.toString()},
        process.env.JWT_SECRET || 'secret'
    );
    user.tokens = user.tokens.concat({ token });
    user.save();
    return token;
}

export function getProfileTreatment(req, res) {
    try {
        sendJsonData(res, {user: req.session.user}, HTTP_STATUS.OK);
    } catch (error) {
        sendJsonError(res, error, HTTP_STATUS.INTERNAL_SERVER_ERROR);
    }
}
export async function deleteUserTreatment(req, res) {
    try {
        await deleteUser(req.session.user._id);
        sendJsonData(res, {}, HTTP_STATUS.OK);
    } catch (error) {
        sendJsonError(res, error, HTTP_STATUS.INTERNAL_SERVER_ERROR);
    }
}

export async function updateUserTreatment(req, res) {
    const updatesKeys = Object.keys(req.body);
    const allowedUpdates = [
        'name',
        'firstName',
        'lastName',
        'email',
        'phone',
        'password',
        'street',
        'city',
        'image'
    ];
    const isValidOperation = updatesKeys.every(key =>
        allowedUpdates.includes(key)
    );
    if (!isValidOperation) {
        return sendJsonError(res, { message: 'Operation isn\'t valid!'}, HTTP_STATUS.BAD_REQUEST);
    }
    try {
        const data = req.body;
        let image;
        if (req.file) {
            image = await saveImage(req.file, req.session.user.email);
            updatesKeys.push('image');
            data.image = image;
        }
        const user = await updateUser(req.session.user._id, updatesKeys, data);
        sendJsonData(res, { user }, HTTP_STATUS.OK)
    } catch (error) {
        sendJsonError(res, error, HTTP_STATUS.INTERNAL_SERVER_ERROR);
    }
}
