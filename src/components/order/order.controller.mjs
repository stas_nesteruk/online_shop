import HTTP_STATUS from 'http-status';
import {
    saveOrder,
    getOrder,
    deleteOrder,
    getAllOrders
} from './order.service';
import { sendJsonData, sendJsonError } from '../../utils/response';

export async function makeOrderTreatment(req, res) {
    console.log('MAKE');
    try {
        const products = [];
        if (req.session.cart.products.length > 0) {
            req.session.cart.products.forEach(data => {
                products.push({
                    productId: data.product._id,
                    image: data.product.image,
                    price: data.product.price,
                    name: data.product.name,
                    count: data.count
                });
            });
        } else {
            return sendJsonError(res, { message: 'Cart is empty' }, HTTP_STATUS.BAD_REQUEST);
        }
        const orderItems = [
            {
                products,
                totalCost: req.session.cart.totalCost
            }
        ];
        await saveOrder(req.session.user._id, orderItems);
        req.session.cart.totalCost = 0;
        req.session.cart.totalCount = 0;
        req.session.cart.products = [];
        const order = {};
        order.products = orderItems[0].products;
        order.totalCost = orderItems[0].totalCost;
        console.log(orderItems[0]);
        sendJsonData(res, { order }, HTTP_STATUS.OK);
    } catch (error) {
        sendJsonError(res, error, HTTP_STATUS.INTERNAL_SERVER_ERROR);
    }
}

export async function getOrderTreatment(req, res) {
    const id = req.params.id;
    try {
        const order = await getOrder(id);
        if(!order){
            return sendJsonError(res, { message: 'Order did\'t find.' }, HTTP_STATUS.NOT_FOUND);
        }
        sendJsonData(res, {order: order.orderItems[0]}, HTTP_STATUS.OK);
    } catch (error) {
        if(error.name === 'CastError'){
            return sendJsonError(res, {message: 'Wrong id number. Id must be a single String of 12 bytes or a string of 24 hex characters.'}, HTTP_STATUS.BAD_REQUEST);
        }
        sendJsonError(res, error, HTTP_STATUS.INTERNAL_SERVER_ERROR);
    }
}

export async function getUserOrdersTreatment(req, res) {
    try {
        const user = req.session.user;
        await user.populate('orders').execPopulate();
        sendJsonData(res, { orders: user.orders }, HTTP_STATUS.OK);
    } catch (error) {
        sendJsonError(res, error, HTTP_STATUS.INTERNAL_SERVER_ERROR);
    }
}

export async function deleteOrderTreatment(req, res) {
    const id = req.params.id;
    try {
        const order = await deleteOrder(id);
        if(!order){
            return sendJsonError(res, {message: 'Order didn\'t find'}, HTTP_STATUS.NOT_FOUND);
        }
        sendJsonData(res, {order: order.orderItems[0]}, HTTP_STATUS.OK);
    } catch (error) {
        if(error.name === 'CastError'){
            return sendJsonError(res, {message: 'Wrong id number. Id must be a single String of 12 bytes or a string of 24 hex characters.'}, HTTP_STATUS.BAD_REQUEST);
        }
        res.status(HTTP_STATUS.INTERNAL_SERVER_ERROR).send(error);
    }
}
