import express from 'express';
import {
    getCategoriesTreatment,
    deleteCategoryTreatment,
    updateCategoryTreatment,
    createCategoryTreatment
} from './category.controller';
import {
    auth,
    checkPermission
} from '../../middleware';

const router = express.Router();

router.get('/categories', getCategoriesTreatment);
router.post('/categories', auth, checkPermission, createCategoryTreatment);

router.delete('/category/:id', auth, checkPermission, deleteCategoryTreatment);
router.patch('/category/:id', auth, checkPermission, updateCategoryTreatment);

export { router as categoryRouter };
