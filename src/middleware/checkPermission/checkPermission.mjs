import HTTP_STATUS from 'http-status';
import { sendJsonError } from '../../utils/response/';

export function checkPermission(req, res, next){
    try{
        const user = req.session.user;
        if(user.role !== 'Administrator'){
            throw new Error();
        }
        next();
    }catch(error){
        sendJsonError(res, {message: 'Wrong action'}, HTTP_STATUS.FORBIDDEN);
    }
}