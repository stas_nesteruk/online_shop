import HTTP_STATUS from 'http-status';
import {
    getProducers,
    saveProducer,
    deleteProducer,
    updateProducer
} from './producer.service';
import { sendJsonData, sendJsonError } from '../../utils/response';

export async function getProducersTreatment(req, res) {
    try {
        const producers = await getProducers();
        sendJsonData(res, { producers }, HTTP_STATUS.OK);
    } catch (error) {
        sendJsonError(res, error, HTTP_STATUS.INTERNAL_SERVER_ERROR);
    }
}

export async function deleteProducerTreatment(req, res) {
    const id = req.params.id;
    try {
        const producer = await deleteProducer(id);
        if(typeof producer === 'undefined'){
            sendJsonError(res, { message: 'Producer didn\'t find'}, HTTP_STATUS.NOT_FOUND);
        }
        sendJsonData(res, { producer }, HTTP_STATUS.OK);
    } catch (error) {
        if(error.name === 'CastError'){
            return sendJsonError(res, {message: 'Wrong id number. Id must be a single String of 12 bytes or a string of 24 hex characters.'}, HTTP_STATUS.BAD_REQUEST);
        }
        sendJsonError(res, error, HTTP_STATUS.INTERNAL_SERVER_ERROR);
    }
}

export async function updateProducerTreatment(req, res) {
    const id = req.params.id;
    try {
        if (!req.body.name) {
            return sendJsonError(res, { message: 'Wrong required field' }, HTTP_STATUS.BAD_REQUEST);
        }
        const producer = await updateProducer(id, req.body.name);
        sendJsonData(res, { producer }, HTTP_STATUS.OK);
    } catch (error) {
        if(error.name === 'CastError'){
            return sendJsonError(res, {message: 'Wrong id number. Id must be a single String of 12 bytes or a string of 24 hex characters.'}, HTTP_STATUS.BAD_REQUEST);
        }  
        sendJsonError(res, error, HTTP_STATUS.INTERNAL_SERVER_ERROR);
    }
}

export async function createProducerTreatment(req, res) {
    try {
        if (!req.body.name) {
            return sendJsonError(res, { message: 'Wrong required field' }, HTTP_STATUS.BAD_REQUEST);
        }
        const producer = await saveProducer(req.body.name);
        sendJsonData(res, { producer }, HTTP_STATUS.CREATED);
    } catch (error) {
        sendJsonError(res, error, HTTP_STATUS.INTERNAL_SERVER_ERROR);
    }
}
