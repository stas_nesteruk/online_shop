import winston from 'winston';
import path from 'path';
import fs from 'fs';

const logsDir = path.join(process.cwd(), '..', LOGGER_CONFIG.logsDir);
const appLogsPath = path.join(logsDir, LOGGER_CONFIG.logFile);
const crashLogsPath = path.join(logsDir, LOGGER_CONFIG.crashLogFile);

const transports = [];

switch (getCurrentEnvironment()) {
  case ENVIRONMENTS.CI:
  case ENVIRONMENTS.DEV:
  case ENVIRONMENTS.TEST:
    transports.push(new (winston.transports.Console)({
      level: 'debug',
      colorize: true,
      handleExceptions: true,
      humanReadableUnhandledException: true,
    }));
    break;
  case ENVIRONMENTS.STAGE:
  case ENVIRONMENTS.PROD:
    if (!fs.existsSync(logsDir)) {
      fs.mkdirSync(logsDir);
    }
    transports.push(
      new (winston.transports.File)({
        name: 'info-file',
        level: 'info',
        filename: appLogsPath,
      }),
      new (winston.transports.File)({
        name: 'crash-file',
        level: 'error',
        filename: crashLogsPath,
        handleExceptions: true,
        humanReadableUnhandledException: true,
      })
    );
    break;
  default:
}

const loggerLevels = {
  levels: {
    error: 0,
    warn: 1,
    info: 2,
    sequelize: 3,
    debug: 4,
  },
  colors: {
    error: 'red',
    warn: 'yellow',
    sequelize: 'blue',
    info: 'green',
    debug: 'yellow',
  },
};

let INSTANCE = null;

export function getLogger() {
  if (INSTANCE === null) {
    INSTANCE = new (winston.Logger)({
      levels: loggerLevels.levels,
      colors: loggerLevels.colors,
      transports,
    });
  }
  return INSTANCE;
}
